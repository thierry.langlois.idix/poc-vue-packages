import {
  EntityType,
  registerEntityType,
  registerAlter
} from "@thierry.langlois.idix/core";

// class ContentEntityType extends EntityType {}

const contentEntityType = new EntityType({
  plugin: "content",
  key: "content",
  routePath: "contents",
  api: "content-plugin/contents",
  label: "Contenu",
  tenantAware: true
});

registerEntityType(contentEntityType);

registerAlter({
  hook: "entity-type-registry",
  exec(registry) {
    const contentEntityType = registry.find(item => item.key === "content");
    if (contentEntityType) {
      contentEntityType.api = "contents";
    }
  }
});
