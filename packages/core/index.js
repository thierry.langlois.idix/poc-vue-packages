import Vue from "vue";
import routerFactory from "./router";
import App from "./App.vue";

export class Entity {
  constructor(entityType, initialData) {
    this.entityType = entityType;
    this.initialData = initialData;
  }

  async generateForm() {}

  async save() {}
}

export class EntityType {
  constructor(definition) {
    for (const [key, value] of Object.entries(definition)) {
      this[key] = value;
    }
  }

  getBaseApiUrl(parent = "-") {
    const { tenantAware } = this;
    const url = [];
    if (tenantAware) {
      url.push("tenants", "eda");
    }
    url.push(this.api);
    return url.join("/");
  }

  async load(id) {
    const apiUrl = [this.getBaseApiUrl(), id].join("/");
    const data = await apiClient.get(apiUrl);
    return this.createEntity(data);
  }

  async loadMultiple() {}

  createEntity(data) {
    return new Entity(this, data);
  }
}

const entityTypeRegistry = [];
export const registerEntityType = entityType => {
  entityTypeRegistry.push(entityType);
};

const alters = [];
export const registerAlter = alter => {
  alters.push(alter);
};

function triggerAlter(hook, value) {
  alters
    .filter(alter => alter.hook === hook)
    .forEach(alter => {
      alter.exec(value);
    });
  return value;
}

export default async () => {
  triggerAlter("entity-type-registry", entityTypeRegistry);

  Vue.prototype.$entities = entityTypeRegistry;

  Vue.config.productionTip = false;
  console.log(entityTypeRegistry);
  new Vue({
    router: routerFactory({ entityTypeRegistry }),
    render: h => h(App)
  }).$mount("#app");
};
