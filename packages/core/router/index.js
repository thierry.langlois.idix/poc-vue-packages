import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

export default ({ entityTypeRegistry }) => {
  const entityTypeRoutes = entityTypeRegistry.map(entityType => {
    const { key, routePath } = entityType;
    return {
      name: `${key}-admin-list`,
      path: `/${routePath}`,
      component: () => import("../components/HelloWorld.vue"),
      props: {
        entityType
      }
    };
  });

  const router = new VueRouter({
    routes: [
      {
        path: "/",
        name: "home",
        component: () => import("../components/HelloWorld.vue")
      },
      ...entityTypeRoutes
    ],
    mode: "history"
  });

  return router;
};
